#include <iostream>
#include "myclass.h"

using std::cout;
using std::endl;

int main() {
  {
    //constructor check - ok
    char s[] = "hello";
    String str(s);
    cout << "1) " << str << endl;
  }

  {
    // = check - ok
    String str = "abcdef";
    cout << "2) " << str << endl;
  }

  {
    //conatenation - ok
    String str = "hello";
    String str1 = "world";
    String str2 = str + str1;
    cout << "3) " << str2 << endl;
    cout << "3) " << str + str1 << endl;
  }

  {
    //conatenation - ok
    String str = "hello";
    String str1 = str + " world";
    cout << "4) " << str1 << endl;
    cout << "4) " << str + " world" << endl;
  }

  {
    //concatenation - ok
    String str = "world";
    String str1 = "hello " + str;
    cout << "5) " << str1 << endl;
    cout << "5) " << "hello " + str << endl;
  }

  {
    // += - ok
    String str1 = "Abcde";
    String str2 = "Fgkh";
    str1 += str2;
    cout << "6) " << (str1 += str2) << endl;
    cout << "6) " << str1 << endl;
  }

  {
    //multiplication - ok
    String str1 = "Abcde";
    String s = str1 * 3;
    cout << "7) " << s << endl;
    cout << "7) " << str1 * 3 << endl;
  }

  {
    //multiplication *= - ok
    String str1 = "dfdfj";
    cout << "8) " << (str1 *= 4) << endl;
    cout << "8) " << str1 << endl;
  }

  {
    //multiplication - ok
    String str1 = "yhjdk";
    String str2 = 4 * str1;
    cout << "9) " << 4 * str1 << endl;
    cout << "9) " << str2 << endl;
  }

  {
    //strCat - ok
    char s[] = ". hello";
    char d[] = " world .";
    cout << "10) " << strCat(s, d) << endl;
    cout << "10)) " << s << endl;
    cout << "10) " << sum(s, d) << endl;
  }

  {
    //strLen - ok
    char s[] = " hello";
    cout << "11) " << strLen(s) << endl;
  }

  {
    //strCpy - ok
    char d[10];
    char s[] = "hello";
    strCpy(d, s);
    cout << "12) " << d << endl;
  }


  return 0;
}
