#include <iostream>
#include "myclass.h"

using std::cout;
using std::endl;

String::String() {
  str = 0;
}

String::String(const char* s) {
  str = new char[strLen(s) + 1];
  strCpy(str, s);
}

String::String(const String& s) {
  str = new char[strLen(s.str) + 1];
  strCpy(str, s.str);
}

String::~String() {
  if (str) {
    delete [] str;
  }
}

String String::operator= (const char* s) {
  if (str) {
    delete [] str;
  }
  str = new char[strLen(s) + 1];
  strCpy(str, s);
}

String String::operator+ (const String s) {
  String* temp = new String();
  temp->str = sum(str, s.str);
  return *temp;
}

String String::operator+ (const char* s) {
  String* temp = new String();
  temp->str = sum(str, s);
  return *temp;
}

String operator+ (const char* s, String str) {
  String* temp = new String();
  temp->str = sum(s, str.str);
  return *temp;
}

String String::operator+= (const String s) {
  char* temp = sum(str, s.str);
  if (str) {
    delete [] str;
  }
  str = temp;
  return *this;
}

String String::operator+= (const char* s) {
  char* temp = sum(str, s);
  if (str) {
    delete [] str;
  }
  str = temp;
  return *this;
}

String String::operator* (int value) {
  String* temp = new String();
  temp->str = mul(str, value);
  return *temp;
}

String String::operator*= (int value) {
  char* temp = mul(str, value);
  if (str) {
    delete [] str;
  }
  str = temp;
  return *this;
}

String operator* (int value, String s) {
  return s * value;
}

ostream& operator<< (ostream& os, const String s) {
  os << s.str;
  return os;
}

int strLen(const char* str) {
  int size = 0;
  while (str[size]) {
    ++size;
  }
  return size;
}

void strCpy(char* str, const char* s) {
  for (int i = 0; i <= strLen(s); ++i) {
    str[i] = s[i];
  }
}

char* strCat(char* str, const char* s) {
  char* temp = new char[strLen(str) + 1];
  strCpy(temp, str);
  str = new char[strLen(temp) + strLen(s) + 1];
  int size = strLen(temp);
  strCpy(str, temp);
  delete [] temp;
  for (int i = 0; i <= strLen(s); ++i) {
    str[i + size] = s[i];
  }
  return str;
}

char* sum(const char* s1, const char* s2) {
  char* temp = new char[strLen(s1) + strLen(s2) + 1];
  strCpy(temp, s1);
  temp = strCat(temp, s2);
  return temp;
}

char* mul(char* s, int value) {
  char* temp = new char[strLen(s) * value + 1];
  strCpy(temp, s);
  for (int i = 0; i < value - 1; ++i) {
    temp = strCat(temp, s);
  }
  return temp;
}
