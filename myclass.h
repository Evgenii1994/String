#include <iostream>

using std::ostream;
using std::istream;

class String{
  char* str;

public:
  String();
  String(const char* s);
  String(const String& s);

  ~String();

  String operator= (const char* s);

  String operator+ (const String s);
  String operator+ (const char* s);
  friend String operator+ (const char* s, String str);
  String operator+= (const String s);
  String operator+= (const char* s);

  String operator* (const int value);
  String operator*= (const int value);
  friend String operator* (int value, String s);

  bool operator< (const String s);
  bool operator<= (const String s);
  bool operator> (const String s);
  bool operator>= (const String s);
  bool operator!= (const String s);
  bool operator== (const String s);

  friend ostream& operator<< (ostream& os, const String s);
  friend istream& operator>> (istream& is, const String s);
};

void strCpy(char* str, const char* s);
int strLen(const char* str);
char* strCat(char* str, const char* s);
char* sum(const char* s1, const char* s2);
char* mul(char* s, int value);
